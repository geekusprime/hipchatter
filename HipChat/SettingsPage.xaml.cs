﻿using HipChat.Libs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace HipChat
{
    /// <summary>
    /// Pagina vuota che può essere utilizzata autonomamente oppure esplorata all'interno di un frame.
    /// </summary>
    public sealed partial class SettingsPage : Page
    {
        public SettingsPage()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Richiamato quando la pagina sta per essere visualizzata in un Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            pushNotificationsSwitch.IsOn = Account.Current.HasPushNotificationsEnabled;
            jumpRecentsToggle.IsOn = Account.Current.ShouldJumpToRecents;

            GoogleAnalytics.EasyTracker.GetTracker().SendView("settings");
        }

        private void Confirm_Tapped(object sender, RoutedEventArgs e)
        {
            if (feedbackMessage.Text.Length > 0)
            {
                SendMessage_Click(null, null);
            }

            Account.Current.HasPushNotificationsEnabled = pushNotificationsSwitch.IsOn;
            Account.Current.ShouldJumpToRecents = jumpRecentsToggle.IsOn;

            if (Frame.CanGoBack)
            {
                Frame.GoBack();
            }
            else
            {
                Frame.Navigate(typeof(MainPage));
            }
        }

        private void ShowTutorial_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(TutorialPage));
        }

        private void SendMessage_Click(object sender, RoutedEventArgs e)
        {
            if (feedbackMessage.Text.Length > 0)
            {
                CloudSocket.SendFeedback(feedbackMessage.Text);

                feedbackFlyout.Hide();
                feedbackMessage.Text = "";
            }
        }

    }
}
