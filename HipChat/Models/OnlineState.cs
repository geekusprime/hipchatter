﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HipChat
{
    public enum OnlineState
    {
        Online,
        Offline,
        Away,
        ExtendedAway,
        DoNotDisturb,
        Chat,
        OnMobileApple,
        OnMobileAndroid,
        PublicRoom
    }
}
